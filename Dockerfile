# Build webpack
FROM registry.gitlab.com/dedyms/buster-slim:node14-dev AS frontend
ENV QT_QPA_PLATFORM=offscreen
RUN apt update && apt install -y libssl-dev libsqlite3-dev phantomjs git ca-certificates make python && \
    apt clean && \
    rm -rf /var/lib/apt/lists/*
RUN git clone -b develop --depth=1 https://github.com/hackmdio/codimd.git /codimd
WORKDIR /codimd
#RUN sed -i 's/npm/npm -d/g' bin/setup
RUN rm package-lock.json && \
    npm config set registry https://registry.npmjs.org/ && \
    ./bin/setup && \
    npm -d run build

# Install prod
FROM registry.gitlab.com/dedyms/buster-slim:node14-dev AS backend
ENV QT_QPA_PLATFORM=offscreen
RUN apt update && apt install -y libssl-dev libsqlite3-dev phantomjs git ca-certificates make python && \
    apt clean && \
    rm -rf /var/lib/apt/lists/*
RUN git clone -b develop --depth=1 https://github.com/hackmdio/codimd.git /codimd
WORKDIR /codimd
RUN rm package-lock.json && \
    npm config set registry https://registry.npmjs.org/ && \
    npm install --only=prod && \
    rm -rf ./public
RUN rm -rf .git .gitignore .travis.yml .dockerignore .editorconfig .babelrc .mailmap .sequelizerc.example \
        test docs contribute \
        package-lock.json webpack.prod.js webpack.htmlexport.js webpack.dev.js webpack.common.js \
        config.json.example README.md CONTRIBUTING.md AUTHORS

# extract out
FROM registry.gitlab.com/dedyms/node:14
USER $CONTAINERUSER
COPY --from=frontend --chown=$CONTAINERUSER:$CONTAINERUSER /codimd/public $HOME/codimd/public
COPY --from=backend --chown=$CONTAINERUSER:$CONTAINERUSER /codimd $HOME/codimd
WORKDIR $HOME/codimd
CMD ["node", "app.js"]
